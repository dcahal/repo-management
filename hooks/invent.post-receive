#!/bin/bash

mgmtdir="/home/git/repo-management"
currpath=$(pwd)

# Initialize the virtualenv we will use (due to certain below scripts having Python dependencies that apt cannot satsify)
source ~/virtualenv/bin/activate

# Determine where we are
urlpath=`python3 $mgmtdir/helpers/extract-gitlab-path.py`

# Log which repository we are working on
echo "*** $urlpath" >> /srv/git/logs/kde-post-receive-hooks.log

# Trigger builds if necessary on the KDE CI system (build.kde.org) and Binary Factory (binary-factory.kde.org)
# Due to a bug in Jenkins, we must not send a notification to the CI system if the repository being changed is one of the infrastructure repositories used to run the CI system
if [[ "$urlpath" != "sysadmin/ci-tooling" ]] && [[ "$urlpath" != "sysadmin/repo-metadata" ]] && [[ "$urlpath" != "frameworks/kapidox" ]] && [[ "$urlpath" != "sdk/kde-dev-scripts" ]]; then
    # Given we aren't one of those repositories, start notifying!
    # Tell the KDE CI system
    curl -s --connect-timeout 2 --max-time 2 "https://build.kde.org/git/notifyCommit?url=https://invent.kde.org/$urlpath" < /dev/null &>> /srv/git/logs/kde-post-receive-hooks.log &
    # Then tell the Binary Factory
    curl -s --connect-timeout 2 --max-time 2 "https://binary-factory.kde.org/git/notifyCommit?url=https://invent.kde.org/$urlpath" < /dev/null &>> /srv/git/logs/kde-post-receive-hooks.log &
fi

# Inform the Neon build system as well, primarily for neon/* repositories (but we send notifications for everything else as that can't harm either)
# This is different to the above because they want branch specific notifications due to quirks in their job setups that can cause build and polling storms
# We cannot do this type of check with the KDE CI system or Binary Factory as they are exclusively pipeline based, and Jenkins Pipelines have various quirks in how they format Git references
# This causes builds not to trigger when they are supposed to - as the reference saved by the Pipeline doesn't match what we send to Jenkins here
# (Due to yet another quirk, there are multiple formats used by the Pipeline system in Jenkins for Git references, making it very difficult to determine what you should send with any degree of reliability)
while read oldrev newrev ref; do
    git log --format=%B -n 1 $ref &> /dev/null
    if [ $? -eq 0 ]; then
        branch=${ref#refs/heads/}
        curl -s --connect-timeout 2 --max-time 2 "https://build.neon.kde.org/git/notifyCommit?url=https://invent.kde.org/$urlpath&branches=${branch}&sha1=${newrev}" < /dev/null &>> /srv/git/logs/kde-post-receive-hooks.log &
    fi
done

# Trigger sync to GitHub
# This script will only sync repositories registered in sysadmin/repo-metadata (after ignoring sysadmin/ and websites/)
# As such we don't need to worry about private repositories being replicated to GitHub
python $mgmtdir/helpers/mirror-to-github.py --metadata-path ~/repo-metadata/projects-invent/ --organization KDE --token `cat ~/github-token` &>> /srv/git/logs/kde-post-receive-hooks.log &

# Wait for the Jenkins and GitHub sync steps above to finish
wait
