#!/usr/bin/env python3

# Load dependencies
import os
import re
import sys
import subprocess
from hooklib import Repository, Changeset, Commit, RepositoryMetadataLoader, CommitAuditor, CommitEmailNotifier, ChangesetEmailNotifier, CiaNotifier, CommitChecker, RepositoryType, ChangeType, RefType, NotificationAllowed

def usage():
    print("Information needed to run could not be gathered successfully.")
    print("Required environment variables: GIT_DIR, GL_USERNAME, HOME")
    print("Stdin: one entry per line following the format: <oldsha> <newsha> <ref>")
    print("Syntax: No parameters required")
    exit(1)

#####
# Settings
#####

# Our repositories, and the repo-management clone can be found at these locations
repository_root = "/srv/git/repositories/"
management_directory = "/home/git/repo-management/"

# Commit URLs in our emails should take this form
Commit.UrlPattern = "https://invent.kde.org/{0}/commit/{1}"

# Always allow these users to perform force pushes
force_pushes_permitted = ['kdecommunity']
# Always allow these users to summarise
commit_summary_exceptions = ['kdecommunity']

# These users maintain the Qt mirror and therefore bypass normal checks relating to those repositories
qt_mirror_bots = ['kdecommunity']

# These repositories accept contributions directly on GitHub, a process that generates merge commits authored by 'GitHub'
# Therefore we have to grant a full name exception for the name 'GitHub' for these repositories
allow_github_merges = [
    'websites/inqlude',
    'websites/inqlude-data',
    'sdk/heaptrack'
]

# These file types are either binary in nature, or otherwise need to be in a specific encoding (otherwise they're corrupt) so we skip EOL checks for them
eol_mimetype_exceptions  = {"text/vcard", "text/x-vcard", "text/directory", "image/svg", "image/x-portable-graymap"}
eol_extension_exceptions = {"vcf", "vcf.ref", "svg", "pdf", "pgm", "fits"}

# The regex rules in this file will be use to restrict the filenames we allow
filenames_blocked_config = os.path.join( management_directory, 'hooks', 'blockedfiles.cfg' )

# These names are accepted and recognised as full names despite lacking a space
full_name_exceptions = [
    'Chaitanya',
    'Shubham',
    'Nyanpasu'
]

# These email domains will be blocked (in addition to ones like localhost and localhost.localdomain)
# Bombardier domains are forbidden because $CORPORATE has problems with their addresses being used publicly
email_domains_blocked = [
    'bombardier.com',
    'rail.bombardier.com',
    'noreply.users.github.com',
    'noreply.users.gitlab.com'
]

# These users maintain the copies of the translations or otherwise need to commit to them in our repositories and are therefore allowed to commit to translation directories
translation_mirror_maintainers = ['scripty', 'aacid', 'ltoscano', 'dfaure']
# Files matching these patterns will be considered to be in translation directories and thus only modifiable by the above people
translation_file_rules = [
    '^po/.*',
    '^poqm/.*'
]

# For these users we always skip notifications
notification_user_exceptions = ["scripty"]

#####
# Initialisation
#####

# Make sure the base directory containing the repositories is present
if not os.path.exists( repository_root ):
    print( "Base directory could not be found")
    exit(1)

# Do we need to make sure GIT_DIR is around?
# With Gitaly this isn't always the case
if 'GIT_DIR' not in os.environ:
    os.environ['GIT_DIR'] = os.getenv('PWD')

# Read needed environment variables
repo_dir  = os.getenv('GIT_DIR')
push_user = os.getenv('GL_USERNAME')
user_home = os.getenv('HOME')

# Initialize the repository
repository = Repository( repository_root, repo_dir, push_user )
# Determine where configuration files specific to this repository would be found
repository_config = os.path.join( management_directory, 'repo-configs', repository.path )

# Read in all of the necessary arguments and setup the changesets
for changesetEntry in sys.stdin:
    # Unpack our values
    old_sha1, new_sha1, ref_name = changesetEntry.split(' ')
    # Cleanup the values a little
    ref_name = ref_name.strip()
    # Setup a changeset
    repository.add_changeset( ref_name, old_sha1, new_sha1 )

# Test to see if we ended up with any changesets to check
if len(repository.changesets) == 0:
    print("No changes to the repository found despite them being expected")
    exit(1)

# Load our repository metadata (which we will need shortly)
metadata_path = os.path.join( user_home, 'repo-metadata' )
repository_metadata = RepositoryMetadataLoader()
repository_metadata.loadProjectsFromTree( metadata_path )

#####
# Determine whether we will be performing notifications on this repository
#####

# Starting position is that repositories should be considered to be non-notifiable
send_notifications    = NotificationAllowed.Never
send_notifications_to = ""

# If they are known to us in our metadata then they are a mainline repository and should be publicly notified
if repository.path in repository_metadata.KnownRepos:
    send_notifications    = NotificationAllowed.Publicly
    send_notifications_to = "kde-commits@kde.org"

# Neon packaging repositories aren't registered in the metadata but should still be notified on...
elif re.match("^neon/(.+)$", repository.path):
    send_notifications    = NotificationAllowed.Publicly
    send_notifications_to = "neon-commits@kde.org"

# Qt mirror repositories aren't registered in the metadata (as they're not KDE projects) but still need notification as well
# We also have some additional special rules for these repositories enforced further below
elif re.match("^qt/(.+)$", repository.path):
    send_notifications    = NotificationAllowed.Publicly
    send_notifications_to = "kde-commits@kde.org"

# Sysadmin repositories live in the sysadmin/ namespace
# Any repository that isn't registered in the metadata should be considered a private (internal to Sysadmin) repository
elif re.match("^sysadmin/(.+)$", repository.path):
    send_notifications    = NotificationAllowed.Privately
    send_notifications_to = "sysadmin@kde.org"

# The KDE e.V. Board repositories live in their team namespace
# Much like Sysadmin, any of these that aren't registered in the metadata are considered private to the Board
elif re.match("^teams/kde-ev-board/(.+)$", repository.path):
    send_notifications    = NotificationAllowed.Privately
    send_notifications_to = "kde-ev-board@kde.org"

# The Security Team uses forks in their (private) team namespace when handling code reviews for security patches
# Therefore any repository in their namespace should be considered private
elif re.match("^teams/security/(.+)$", repository.path):
    send_notifications    = NotificationAllowed.Privately
    send_notifications_to = "security@kde.org"

# Infrastructure tests repositories are used by sysadmins for testing various bits related to Gitlab
elif re.match("^teams/infrastructuretests/(.+)$", repository.path):
    send_notifications    = NotificationAllowed.Privately
    send_notifications_to = "sysadmin@kde.org"

#####
# Determine whether we need to skip processing because this is a repository Gitlab manages (Wiki, Design and Snippets all use repositories internally)
#####

if repository.repo_type in [RepositoryType.Wiki, RepositoryType.Design, RepositoryType.Snippets]:
    exit(0)

#####
# Auditing - These are done per changeset with few exceptions
#####

# Initialize the commit auditor
auditor = CommitAuditor()

# Will we be allowing merge commits from GitHub?
if repository.path in allow_github_merges:
    full_name_exceptions.append('GitHub')

# Make sure that the notification.summarise option is not being misused
if 'notification.summarise' in repository.push_options and not re.match("^neon/(.+)$", repository.path) and push_user not in commit_summary_exceptions:
    print("Usage of the commit notification summarization option is restricted to Neon repositories only.")
    print("Push declined - attempted unauthorised use of KDE Infrastructure")
    exit(1)

# All repositories under teams/ must be notified on, whether publicly or privately...
# If no notifications will be sent, then disallow this push
if re.match("^teams/(.+)$", repository.path) and send_notifications is NotificationAllowed.Never:
    print("Pushing to a team repository that isn't registered with Sysadmin is not permitted.")
    print("Push declined - attempted unauthorised use of KDE Infrastructure")
    exit(1)

# Now do the per changeset checks...
for changeset in repository.changesets.values():
    # Make sure backup refs cannot be changed
    if changeset.ref_type == RefType.Backup:
        print("Pushing to backup refs is not supported for security reasons")
        print("Push declined - attempted repository integrity violation")
        exit(1)

    # Do the same for refs/merge-requests/*
    elif changeset.ref_type == RefType.MergeRequest:
        print("Pushing to merge requests directly is not permitted")
        print("Please make this change through Gitlab itself")
        print("Push declined - attempted repository integrity violation")
        exit(1)

    # As well as for refs/keep-around/* and any others that are internal
    elif changeset.ref_type == RefType.Internal:
        print("Pushing to server maintained internal references is not permitted")
        print("Push declined - attempted repository integrity violation")
        exit(1)

    # Don't allow any random refs/* entries we don't already recognise to be created/altered/deleted either
    elif changeset.ref_type == RefType.Unknown:
        print("Sorry, but the ref you are trying to push to could not be recognised.")
        print("Only pushes to branches, tags and notes are permitted.")
        exit(1)

    # Make sure people can't create branches/tags/anything else called HEAD as it causes Git to do some very bad things and can break the repository
    elif changeset.ref_name == "HEAD":
        print("Creating refs which conflict with internally used names is not permitted.")
        print("Push declined - attempted repository integrity violation")
        exit(1)

    # Make sure that we don't get branches named 'main' as people are starting to try to switch to it (vs. the normal 'master' branch name)
    # Unfortunately this causes all sorts of issues for certain infrastructure components (especially around i18n)
    elif changeset.ref_name == "main" and changeset.change_type != ChangeType.Delete:
        print("Usage of the alternative name, 'main', for the default branch of projects creates issues for certain components of KDE infrastructure.")
        print("As such, usage of the branch name 'main' is not permitted.")
        print("Push declined - attempted repository integrity violation")
        exit(1)

    # Make sure that people don't try to accidentally create branches starting with origin/*
    # These usually just duplicate branches we already have and make a mess of the repository
    elif changeset.change_type == ChangeType.Create and re.match("^origin/(.+)$", changeset.ref_name):
        print("Creating refs starting with the name origin/ is not permitted.")
        print("This is usually caused by incorrectly pushing a branch or tag.")
        print("Please ensure your remote branch name does not contain 'origin/' at the beginning")
        print("Push declined - attempted repository integrity violation")
        exit(1)

    # Ensure that people don't create work branches that Gitlab will consider to be protected (and thus be protected from force push and deletion)
    # We disallow these as protecting a work branch defeats the purpose of it being a work branch
    elif changeset.change_type == ChangeType.Create and re.match("^work/(.*)\.(.*)$", changeset.ref_name):
        print("By default branches containing '.' are protected on KDE Invent.")
        print("To ensure that it is possible to force push and rebase work branches, we therefore don't allow '.' in work branches.")
        print("Please ensure that your work branch does not contain '.' in it's name.")
        print("Push declined - branch would not be usable as intended")
        exit(1)

    # As a matter of policy, we like our tags to be annotated (which also allows for them to be GPG signed and a few other things)
    if changeset.ref_type == RefType.Tag and changeset.change_type != ChangeType.Delete and changeset.commit_type != "tag":
        print("Pushing an unannotated tag is not permitted.")
        print("Push declined - attempted repository integrity violation")
        exit(1)

    # For the next set of checks, we only do these if we are sending out notifications
    if send_notifications is not NotificationAllowed.Never:
        # Force pushes must be specially allowed
        if changeset.change_type is ChangeType.Forced and changeset.ref_type is not RefType.WorkBranch and not os.path.exists(repository_config + '/allow-force-push') and push_user not in force_pushes_permitted:
            print("Force pushes to mainline KDE repositories is only permitted for specific situations.")
            print("Please contact the KDE Sysadmin team for further assistance")
            exit(1)

        # New commits...
        if len(changeset.commits) > 100 and 'notification.summarise' not in repository.push_options and not os.path.exists(repository_config + "/summarise-notifications"):
            print("More than 100 commits are being pushed")
            print("Push declined - excessive notifications would be sent")
            print("Please file a KDE Sysadmin ticket to continue")
            exit(1)

    # For the Qt repositories, we have some specific checks to run
    if re.match("^qt/(.+)$", repository.path):
        # First, are we a bot that is performing maintenance operations (such as updating the upstream branches/tags)?
        # If so, no need to do any further checks here
        if push_user in qt_mirror_bots:
            continue

        # Ensure that we have a kde/* branch
        if changeset.ref_type is not RefType.Branch or not re.match("^kde/(.*)$", changeset.ref_name):
            print("Only branches named kde/* may be used in the KDE mirrors of the Qt repositories.")
            print("All other branch names (including work/*) are reserved for the use of Qt Project upstream.")
            print("Additionally, modification (including creation) of tags is also restricted and reserved to upstream.")
            print("These upstream branches and tags will be automatically maintained by KDE Invent.")
            print("Push declined - attempted modification of upstream provided content")
            sys.exit(1)

    # Lets check the commits themselves now
    # Start with End of Line Style (LF/CR/CR LF) first
    if not os.path.exists(repository_config + "/skip-eol-checks"):
        auditor.audit_eol( changeset, eol_mimetype_exceptions, eol_extension_exceptions )

    # Then we check filenames...
    if not os.path.exists(repository_config + "/skip-filename-checks"):
        auditor.audit_filename( changeset, filenames_blocked_config )

    # Then author/committer names
    if not os.path.exists(repository_config + "/skip-author-name-checks"):
        auditor.audit_names_in_metadata( changeset, full_name_exceptions )

    # Finally we do author/committer emails
    if not os.path.exists(repository_config + "/skip-author-email-checks"):
        auditor.audit_emails_in_metadata( changeset, email_domains_blocked )

   # Depending on who we are, we may also need to check to see whether we are changing translations that have been mirrored into the repository
   # Only specific users are allowed to change these as they are maintained by scripty
    if not os.path.exists(repository_config + "/skip-translation-protections") and push_user not in translation_mirror_maintainers and not re.match("^neon/(.+)$", repository.path):
        # Review each commit for changes to files...
        for commit in changeset.commits.values():
            # Now check each file that was changed in that commit...
            for filename in commit.files_changed:
                # Check to see if this file matches any of the rules we have for translation files
                for restriction in translation_file_rules:
                    # If the rule matches, then block this commit
                    if re.match(restriction, filename):
                        auditor.log_failure(commit.sha1, "Translations maintained separately: " + filename)

# Did we have any commit audit failures?
if auditor.audit_failed:
    print("Push declined - commits failed audit")
    print("Should the audit failures above mention issues regarding your name, please ensure that your Git username and email match your MyKDE full name and email.")
    print("Please see https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup for more details on ensuring Git has been fully configured.")
    print("In the event that your full name has been set and is shown above as being rejected, please file a Sysadmin ticket at https://go.kde.org/systickets")
    exit(1)

#####
# Pre-acceptance
#####

# Do we need to back it up??
for changeset in repository.changesets.values():
    if changeset.change_type == ChangeType.Forced or changeset.change_type == ChangeType.Delete:
        changeset.backup_ref()

#####
# Post acceptance - Print Nice URLs
#####

# Is this user excluded from notifications?
if push_user in notification_user_exceptions:
    exit(0)

# Output a helpful url to the top most commit for each changeset we have
# Start with a greeting message
print("The commits in this series can be viewed at:")
# Then output a helpful url for each of the changesets we have been given
for changeset in repository.changesets.values():
    # Is this changeset introducing a new commit?
    if changeset.new_sha1 not in changeset.commits:
        continue

    # Output an appropriate message
    print( changeset.commits[ changeset.new_sha1 ].url )

######
# Notifications - Commit Emails, Bugzilla, IRC
######

# Are we allowed to send notifications on this repo?
if send_notifications is NotificationAllowed.Never:
    exit(0)

# Prepare to send notifications
cia = CiaNotifier()
processed_commits = []

# Go over each changeset in turn and notify on the new commits in each one
for changeset in repository.changesets.values():
    # First, before we go any further - Is this change to a work branch?
    if changeset.ref_type is RefType.WorkBranch:
        print("Not processing commit hooks - this is a work branch")
        continue

    for (commit, diff) in changeset.commits_with_diff():
        # Make sure we haven't seen this commit already
        if commit.sha1 in processed_commits:
            continue

        # Ensure we aren't supposed to be summarising this change
        if 'notification.summarise' in repository.push_options or os.path.exists(repository_config + "/summarise-notifications"):
            continue

        # Check for license, etc problems in the commit
        checker = CommitChecker()
        checker.check_commit_problems( commit, diff )

        # Prepare to send out email notifications...
        notifier = CommitEmailNotifier( commit, checker, include_url=True )
        notifier.determine_keywords()

        # Send email notifications for this commit
        notifier.notify_email( send_notifications_to, diff )

        # Handle Bugzilla
        # Because this is a public part of our infrastructure, only do this for public notification repositories
        if send_notifications is NotificationAllowed.Publicly:
            notifier.notify_bugzilla()

        # If the repository permits public notifications then send announcements to IRC
        if send_notifications is NotificationAllowed.Publicly:
            cia.notify( commit )

        # Add this commit to the list of commits we've processed
        processed_commits.append( commit.sha1 )

    # Perform summarised changeset notifications
    # Are we supposed to be doing these?
    if 'notification.summarise' in repository.push_options or os.path.exists(repository_config + "/summarise-notifications"):
        # Send the changeset summary
        notifier = ChangesetEmailNotifier()
        notifier.notify_summary_email( changeset, send_notifications_to )

# Everything is done....
exit(0)
